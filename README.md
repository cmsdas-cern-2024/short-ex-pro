# short-ex-pro
Welcome to the proton short exercise at the CMS DAS 2024!


## Getting started
The exercise is organized in a Jupyter notebook and can be executed using regular CERN resources like SWAN (Service for Web based ANalysis):

- Open a [SWAN session](https://swan.cern.ch) (pick software stack 103, and keep the other default settings)
- In the SWAN session, click on the item on the right-hand side that says "Download Project from git", copy-paste `https://gitlab.cern.ch/CMSDAS-CERN-2024/short-ex-pro.git`
- Open the notebook `Understanding-PPS-protons.ipyn` and follow the instructions there.

## Useful links
- CMSDAS@CERN2024: [https://indico.cern.ch/event/1388937/](https://indico.cern.ch/event/1388937/)
- Introductory slides: [ShortExerciseSlides.pptx](https://gitlab.cern.ch/cmsdas-cern-2024/short-ex-pro/-/blob/master/slides/ShortExerciseSlides.pptx)
- Introduction page maintained by Proton POG with all up-to-date information about the PPS sub-detector: [TaggedProtonsGettingStarted](https://twiki.cern.ch/twiki/bin/view/CMS/TaggedProtonsGettingStarted)
- Proton POG recommendations page: [TaggedProtonsPOGRecommendations](https://twiki.cern.ch/twiki/bin/view/CMS/TaggedProtonsPOGRecommendations)

